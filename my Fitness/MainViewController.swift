//
//  MainViewController.swift
//  my Fitness
//
//  Created by Student on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBOutlet weak var breakfastTF: UITextField!
    
    @IBOutlet weak var lunchTF: UITextField!
    
    @IBOutlet weak var snacksTF: UITextField!
    
    @IBOutlet weak var supperTF: UITextField!
    
    @IBAction func addFood(_ sender: Any) {
        var totalCalories = 0.0
        let breakfast = breakfastTF.text
        let lunch = lunchTF.text
        let snack = snacksTF.text
        let supper = supperTF.text
        
        let products = FoodAbstinence.foodAbstinence.productsDataStore.find() as! [Product]
        print(products.count)
        for product in products {
            if(product.name == breakfast) {
                totalCalories += product.calorie
            }
            if(product.name == lunch) {
                totalCalories += product.calorie
            }
            if(product.name == snack) {
                totalCalories += product.calorie
            }
            if(product.name == supper) {
                totalCalories += product.calorie
            }
        }
        if totalCalories > 3500.0 {
            weightLBL.text = "\(totalCalories/3500.0) increased"
        } else {
            weightLBL.text = "\(totalCalories/3500.0) decreased"
        }
        breakfastTF.text = ""
        lunchTF.text = ""
        snacksTF.text = ""
        supperTF.text = ""
    }
    
    @IBOutlet weak var weightLBL: UILabel!
}
