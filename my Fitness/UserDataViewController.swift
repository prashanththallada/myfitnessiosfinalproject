//
//  UserDataViewController.swift
//  my Fitness
//
//  Created by student on 11/28/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class UserDataViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var currentWeightTF: UITextField!
    
    @IBOutlet weak var goalWeightTF: UITextField!
    
    @IBAction func submit(_ sender: UIButton) {
        if let currentWeightString = currentWeightTF.text, let goalWeightString = goalWeightTF.text {
            if let currentWeight = Double(currentWeightString), let goalWeight = Double(goalWeightString) {
                UserData.userData.updatedGoalWeight(goalWeight: goalWeight)
                UserData.userData.updatedOriginalWeight(originalWeight: currentWeight)
            }
        }
        
    }
    
}
