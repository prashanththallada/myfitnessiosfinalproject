//
//  MotivationViewController.swift
//  my Fitness
//
//  Created by Student on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class MotivationViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var quotesTF: UITextView!
    
    @IBAction func generate(_ sender: UIButton) {
        let randomNum = arc4random_uniform(30)
        switch randomNum {
        case 0: quotesTF.text = "Start somewhere -Anonymous"
        case 1: quotesTF.text = "A healthy outside starts from inside -Robert Urich"
        case 2: quotesTF.text = "Love yourself enough to live a healthy lifestyle -Jules Robson"
        case 3: quotesTF.text = "Health is not valued till sickness comes -Thomas Fuller"
        case 4: quotesTF.text = "Health is the greatest gift -Buddha"
        case 5: quotesTF.text = "You cannot enjoy wealth if you are not in good health -Anonymous"
        case 6: quotesTF.text = "The first wealth is health -Ralph Waldo Emerson"
        case 7: quotesTF.text = "Your body deserves the best -Anonymous"
        case 8: quotesTF.text = "A healthy mind doesn't speak ill of others -Anonymous"
        case 9: quotesTF.text = "Health requires healthy food -Roger Williams"
        case 10: quotesTF.text = "Good food healthy life -Anonymous"
        case 11: quotesTF.text = "The past cannot be changed.  The future is yet in your power -Mary Pickford"
        case 12: quotesTF.text = "It always seems impossible until it's done -Nelson Mandela"
        case 13: quotesTF.text = "Failure will never overtake me if my determination to succeed is strong enough -Og Mandino"
        case 14: quotesTF.text = "Change your life today. Don't gamble on the future, act now, without delay -Simone de Beauvoir"
        case 15: quotesTF.text = "With the new day comes new strength and new thoughts -Eleanor Roosevelt"
        case 16: quotesTF.text = "Only I can change my life. No one can do it for me -Carol Burnett"
        case 17: quotesTF.text = "You are confined only by the walls you build yourself"
        case 18: quotesTF.text = "All progress takes place outside the comfort zone -Michael John Bobak"
        case 19: quotesTF.text = "Success seems to be connected with action. Successful people keep moving. They make mistakes, but they don’t quit -Conrad Hilton"
        case 20: quotesTF.text = "If you can dream it, you can do it -Walt Disney"
        case 21: quotesTF.text = "There are no shortcuts to any place worth going -Beverly Sills"
        case 22: quotesTF.text = "No great achiever – even those who made it seem easy – ever succeeded without hard work -Jonathan Sacks"
        case 23: quotesTF.text = "It is not the strongest of the species that survive, nor the most intelligent, but the one most responsive to change -Charles Darwin"
        case 24: quotesTF.text = "Success is the sum of small efforts, repeated day in and day out -Robert Collier"
        case 25: quotesTF.text = "Always bear in mind that your own resolution to success is more important than any other one thing -Abraham Lincoln"
        case 26: quotesTF.text = "Good health and good sense are two of life's greatest blessings -Publilius Syrus"
        case 27: quotesTF.text = "To keep the body in good health is a duty... otherwise we shall not be able to keep our mind strong and clear -Buddha"
        case 28: quotesTF.text = "It is health that is real wealth and not pieces of gold and silver -Gandhi"
        case 29: quotesTF.text = "Time and health are two precious assets that we don't recognize and appreciate until they have been depleted -Denis Waitley"
        case 30: quotesTF.text = "The only way to keep your health is to eat what you don't want, drink what you don't like, and do what you'd rather not -Mark Twain"
        default: quotesTF.text = "Tomorrow starts today"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        quotesTF.text = "Quotes will be displayed here use Button to generate quotes"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
