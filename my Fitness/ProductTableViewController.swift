//
//  DiaryTableViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class ProductTableViewController: UITableViewController {
    
    var foodAbstinence:FoodAbstinence!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(productsForSelectedProductTypeRetrieved), name: .ProductsForSelectedProductTypeRetrieved, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = foodAbstinence.selectedProductType?.name!
        foodAbstinence.retrieveProductsForSelectedProductType()
    }
    
    @objc func productsForSelectedProductTypeRetrieved() {
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodAbstinence.numProducts()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "products", for: indexPath)
        let productLBL = cell.viewWithTag(20) as? UILabel
        
        productLBL?.text = foodAbstinence.products[indexPath.row].name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let productVC = segue.destination as! ProductViewController
        productVC.products = foodAbstinence.products[tableView.indexPathForSelectedRow!.row]
    }
    
    
}
