//
//  MyTabBarViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 11/28/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class MyTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 2
    }
    
}
