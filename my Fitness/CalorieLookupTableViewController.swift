//
//  CalorieLookupTableViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class CalorieLookupTableViewController: UITableViewController {
    
    
    var foodAbstinence:FoodAbstinence!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        foodAbstinence = FoodAbstinence.foodAbstinence
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(productTypeRetrieved), name: .ProductTypeRetrieved, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        foodAbstinence.retrieveAllProductType()
        tableView.reloadData()
    }
    
    @objc func productTypeRetrieved(){
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodAbstinence.numProductTypes()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let productsLBL = cell.viewWithTag(2) as? UILabel
        productsLBL?.text = foodAbstinence[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        foodAbstinence.selectedProductType = foodAbstinence[indexPath.row]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "productType" {
            let productTVC = segue.destination as! ProductTableViewController
            productTVC.foodAbstinence = foodAbstinence
        }
        
    }
    
}
