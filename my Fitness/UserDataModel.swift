//
//  UserDataModel.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 11/28/18.
//  Copyright © 2018 Student. All rights reserved.
//

import Foundation

//@objcMembers
class UserData {
    let backendless = Backendless.sharedInstance()!
    var originalWeight:Double
    var updatedWeights:[Double] = []
    var goalWeight:Double
    
    static var userData = UserData()
    
    init(originalWeight:Double, updatedWeights:[Double], goalWeight:Double) {
        self.originalWeight = originalWeight
        self.updatedWeights = updatedWeights
        self.goalWeight = goalWeight
    }
    
    convenience init() {
        self.init(originalWeight:0.0, updatedWeights:[], goalWeight:0.0)
    }
    
    func updatedOriginalWeight(originalWeight:Double) {
        self.originalWeight = originalWeight
    }
    
    func updatedGoalWeight(goalWeight:Double) {
        self.goalWeight = goalWeight
    }
    
    func addWeight(weight:Double) {
        updatedWeights.append(weight)
    }
}

