//
//  Product.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 11/5/18.
//  Copyright © 2018 Student. All rights reserved.
//

import Foundation

@objcMembers
class Product : NSObject {
    var name:String?
    var calorie:Double
    override var description: String {
        return "Name: \(name ?? ""), Calories: \(calorie)"
    }
    
    var objectId:String?
    
    init(name:String, calorie:Double) {
        self.name = name
        self.calorie = calorie
    }
    
    convenience override init(){
        self.init(name:"", calorie:0)
    }
}
