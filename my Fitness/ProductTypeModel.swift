//
//  MyFitnessModel.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/20/18.
//  Copyright © 2018 Student. All rights reserved.
//

import Foundation



@objcMembers
class ProductType : NSObject {
    var name:String?
    var products:[Product]
    override var description: String {
        return "Name: \(name ?? ""), ObjectId: \(objectId ?? "N/A")"
    }
    
    var objectId:String?
    
    init(name:String, products:[Product]){
        self.name = name
        self.products = products
    }
    
    convenience override init(){
        self.init(name:"", products:[])
    }
}



