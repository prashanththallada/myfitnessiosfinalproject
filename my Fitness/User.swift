//
//  User.swift
//  my Fitness
//
//  Created by Student on 12/4/18.
//  Copyright © 2018 Student. All rights reserved.
//

import Foundation
struct User {
    var name:String
    var email:String
    var password:String
}

struct Storage{
    
    static var users = Storage()
    private var users:[User]
    
    init() {
        users = [User(name:"Snohi",email:"snohithareddy@gmail.com",password:"Snohi@2911")]
    }
    
    mutating func addUser(_ user:User) {
        users.append(user)
    }
    
    func numOfUsers() -> Int {
        return users.count
    }
    
    func user(_ index:Int) -> User{
        return users[index]
    }
    
    
}


