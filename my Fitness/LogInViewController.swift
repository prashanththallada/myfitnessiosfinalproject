//
//  LogInViewController.swift
//  my Fitness
//
//  Created by Student on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailtf: UITextField!
    
    @IBOutlet weak var pwtf: UITextField!
    
    
    @IBAction func LoginBTN(_ sender: Any) {
        Backendless.sharedInstance()?.userService.login(emailtf.text!, password: pwtf.text!,
                                                        response: { user in
                                                            if user != nil
                                                            {
                                                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "homePage") as! UITabBarController
                                                                self.present(nextViewController, animated:true, completion:nil)
                                                            }
                                                            
        }, error: {fault in
            self.display(title: "Check Your Credentials", msg: (fault?.message!)!)
            
            
        })
        
    }
    
    func display(title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func SignBTN(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        FoodAbstinence.foodAbstinence.saveAllProductTypes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
