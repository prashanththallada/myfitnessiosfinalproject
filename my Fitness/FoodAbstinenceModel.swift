//
//  FoodAbstinenceModel.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 11/5/18.
//  Copyright © 2018 Student. All rights reserved.
//

import Foundation


extension Notification.Name {
    static let ProductTypeRetrieved = Notification.Name("Product Type Retrieved")
    static let ProductsForSelectedProductTypeRetrieved = Notification.Name("Products for Selected Product Type Retrieved")
}



class FoodAbstinence {
    
    
    
    let backendless = Backendless.sharedInstance()
    var productTypeDataStore:IDataStore!
    var productsDataStore:IDataStore!
    
    static var foodAbstinence = FoodAbstinence()
    
    
    var products:[Product] = []
    var productTypes:[ProductType] = []
    private var initialProductTypes:[ProductType] = [
        ProductType(name: "Dairy", products: [
            Product(name: "Milk", calorie: 10),
            Product(name: "Butter", calorie: 130),
            Product(name: "Cheese", calorie: 100),
            Product(name: "Clotted Cream", calorie: 200)]),
        
        ProductType(name: "Proteins", products: [
            Product(name: "Eggs", calorie: 50),
            Product(name: "Almonds", calorie: 25),
            Product(name: "Chicken Breast", calorie: 125)]),
        
        ProductType(name: "Fruits", products: [
            Product(name: "StrawBerry", calorie: 30),
            Product(name: "Avocado", calorie: 120),
            Product(name: "Apricots", calorie: 300)]),
        
        ProductType(name: "Veggies", products: [
            Product(name: "Lettuce", calorie: 90),
            Product(name: "Zucchini", calorie: 150),
            Product(name: "Asparagus", calorie: 340),
            Product(name: "Tomatoes", calorie: 35)])
    ]
    
    var selectedProductType:ProductType?
    var productsForSelectedProductType:[Product] = []
    
    private init() {
        productsDataStore = backendless?.data.of(Product.self)
        productTypeDataStore = backendless?.data.of(ProductType.self)
    }
    
    subscript(_ index:Int) -> ProductType {
        return productTypes[index]
    }
    
    func numProductTypes() -> Int {
        return productTypes.count
    }
    
    func numProducts() -> Int {
        return products.count
    }
    
    func numProductsForSelectedProductType() -> Int {
        return productsForSelectedProductType.count
    }
    
    //retrieveing all products asynchronously
    func retrieveAllProductType() {
        let queryBuilder:DataQueryBuilder = DataQueryBuilder()
        queryBuilder.setRelated(["products"])
        
        queryBuilder.setPageSize(100)
        productTypeDataStore.find(queryBuilder, response: {(results) -> Void in
            self.productTypes = results as! [ProductType]
            NotificationCenter.default.post(name: .ProductTypeRetrieved, object: nil)
        }, error: {(exception) -> Void in
            print(exception.debugDescription)})
    }
    
    //retrieveing a certain product for a product type
    func retrieveProductsForSelectedProductType() {
        let qb:DataQueryBuilder = DataQueryBuilder()
        qb.setWhereClause("name = '\(self.selectedProductType?.name! ?? "")'")
        
        qb.setRelated(["products"])
        qb.setPageSize(100)
        self.productTypeDataStore.find(qb, response: {(results) -> Void in
            let productType = results![0] as! ProductType
            self.products = productType.products
            NotificationCenter.default.post(name: .ProductsForSelectedProductTypeRetrieved, object:nil)
        }, error:{(exception) -> Void in
            print(exception.debugDescription)
        })
    }
    
    // saving all products asynchronously
    func saveAllProductTypes() {
        
        let myQueue = DispatchQueue(label:"myQueue", qos:.utility)
        myQueue.async {
            Types.tryblock({
                for productType in self.initialProductTypes {
                    let savedProductType = self.productTypeDataStore.save(productType) as! ProductType
                    productType.objectId = savedProductType.objectId
                    
                    var productsIds:[String] = []
                    for product in productType.products {
                        let savedProduct = self.productsDataStore.save(product) as! Product
                        productsIds.append(savedProduct.objectId!)
                    }
                    
                    self.productTypeDataStore.addRelation("products:Product:n", parentObjectId: productType.objectId, childObjects: productsIds)
                }
            }, catchblock: {(exception) in
                print(exception ?? "Eh, what's up, doc?")})
        }
    }
    
}
