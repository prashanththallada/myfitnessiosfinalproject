//
//  SignUpViewController.swift
//  my Fitness
//
//  Created by Student on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var Email: UITextField!
    
    
    @IBOutlet weak var EntrPw: UITextField!
    
    
    @IBOutlet weak var confrmPW: UITextField!
    
    @IBOutlet weak var Nametf: UITextField!
    
    @IBAction func Create(_ sender: Any) {
        signUpUserAsync()
    }
    
    func signUpUserAsync()
    {
        let user = BackendlessUser()
        let backendless = Backendless.sharedInstance()
        user.setProperty("name", object: String(describing:Nametf.text!))
        user.setProperty("email", object: String(describing:self.Email.text!))
        
        if(String(describing: self.EntrPw.text!) as NSString == String(describing: self.confrmPW.text!) as NSString)
        {
            user.password = String(describing: self.EntrPw.text!) as NSString
            print(String(self.EntrPw.text!))
            print(String(self.confrmPW.text!))
        }
        else{
            Display(userMessage: "Password doesn't match")
        }
        
        
        backendless!.userService.register(user, response: { ( signedupUser : BackendlessUser!) -> () in
            self.Display(userMessage: "User registered")
        },
                                          error: { (fault : Fault!) -> () in
                                            print("server error: \(String(describing: fault))")
        })
        
    }
    
    func Display(userMessage:String)
    {
        let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated:true, completion:nil)
    }
    
    func displayAfterSignUp(msg: String){
        let alert = UIAlertController(title: "SignUp Completed", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler:{  _ in
            self.performSegue(withIdentifier: "Complete", sender: nil)
        }
        ))
    }
    
    
    @IBAction func ReturnPg(_ sender: Any) {
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
