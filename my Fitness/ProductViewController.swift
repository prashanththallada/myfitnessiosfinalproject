//
//  DiaryViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    @IBOutlet weak var textLBL: UILabel!
    
    var products:Product!
    
    override func viewWillAppear(_ animated: Bool) {
        navigationItem.title = products.name
        textLBL.text = "\(products.calorie)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
