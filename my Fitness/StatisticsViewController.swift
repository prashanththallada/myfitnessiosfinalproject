//
//  StatisticsViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit
import Charts

class StatisticsViewController: UIViewController {
    
    @IBOutlet weak var barChart: BarChartView!
    
    @IBOutlet weak var lineChart: LineChartView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLineChart()
        
        barChart.isHidden = true
        lineChart.isHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        var data = [UserData.userData.originalWeight]
        
        for updatedData in UserData.userData.updatedWeights {
            data.append(updatedData)
        }
        setLineChart(data)
        
        barChart.isHidden = true
        lineChart.isHidden = false
    }
    
    
    var months: [String]!
    
    static var xAxisData: [String] = ["1"]
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        barChart.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        let chartData = BarChartData(dataSet: chartDataSet)
        barChart.data = chartData
        
        barChart.xAxis.labelPosition = .bottom
        barChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
    }
    
    @IBAction func barChartViewBTN(_ sender: UIButton) {
        
        var data = [UserData.userData.originalWeight]
        
        for updatedData in UserData.userData.updatedWeights {
            data.append(updatedData)
        }
        
        lineChart.isHidden = true
        barChart.isHidden = false
        
        
        setChart(dataPoints: StatisticsViewController.xAxisData, values: data)
    }
    
    
    
    
    func setLineChart(_ count:[Double]=[2,3]){
        var values = [ChartDataEntry]()
        
        for data in 0..<count.count {
            values += [ChartDataEntry(x:Double(data), y:count[data])]
        }
        
        let set = LineChartDataSet(values: values, label: "Data set")
        let data = LineChartData(dataSet: set)
        
        self.lineChart.data = data
    }
    
    @IBAction func lineChartViewBTN(_ sender: Any) {
        var data = [UserData.userData.originalWeight]
        
        for updatedData in UserData.userData.updatedWeights {
            data.append(updatedData)
        }
        setLineChart(data)
        
        barChart.isHidden = true
        lineChart.isHidden = false
    }
    
}
