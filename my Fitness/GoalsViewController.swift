//
//  GoalsViewController.swift
//  my Fitness
//
//  Created by Thallada,Prashanth Kumar on 10/6/18.
//  Copyright © 2018 Student. All rights reserved.
//

import UIKit
import Charts

class GoalsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var horizontalBarChart: HorizontalBarChartView!
    
    var counter = 2
    
    @IBOutlet weak var updatedWeightTF: UITextField!
    
    @IBAction func updateWeight(_ sender: UIButton) {
        if let updateWeightString = updatedWeightTF.text {
            if let updateWeight = Double(updateWeightString) {
                UserData.userData.addWeight(weight: updateWeight)
            }
        }
        
        let weights = UserData.userData.updatedWeights
        var totalWeight = 0.0
        var previousWeight = 0.0
        
        for weight in 0..<weights.count {
            if weight != 0 {
                previousWeight = weights[weight-1]
            }
            if previousWeight > weights[weight] {
                let weightToBeAdded = previousWeight - weights[weight]
                totalWeight -= weightToBeAdded
            } else {
                let weightToBeAdded = weights[weight] - previousWeight
                totalWeight += weightToBeAdded
            }
        }
        
        setChartView(values: totalWeight)
        
        StatisticsViewController.xAxisData.append("\(counter)")
        counter += 1
    }
    
    func setChartView(values:Double) {
        horizontalBarChart.xAxis.drawLabelsEnabled = false
        horizontalBarChart.setScaleEnabled(false)
        
        horizontalBarChart.xAxis.drawGridLinesEnabled = false
        horizontalBarChart.leftAxis.gridLineDashLengths = [15.0, 15.0]
        horizontalBarChart.rightAxis.drawGridLinesEnabled = false
        
        var dataEntries: [BarChartDataEntry] = []
        
        
        let dataEntry = BarChartDataEntry(x: 1, y: values)
        dataEntries.append(dataEntry)
        dataEntries.append(BarChartDataEntry(x: 2, y: 0.0))
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: nil)
        let chartData = BarChartData(dataSet: chartDataSet)
        horizontalBarChart.data = chartData
        
        chartDataSet.colors = [UIColor.red]
    }
    
}
